const $table = document.querySelector("#table");
const $score = document.querySelector("#score");

let data = [];
function startGame() {
  const $fragment = document.createDocumentFragment();
  [1, 2, 3, 4].forEach(function () {
    const rowData = [];
    data.push(rowData); //2차원배열 생성 data [ [],[],[],[] ];
    const $tr = document.createElement("tr");
    [1, 2, 3, 4].forEach(() => {
      ~rowData.push(0); // rowData[0,0,0,0 ]
      const $td = document.createElement("td");
      $tr.append($td);
    });
    $fragment.append($tr);
  });
  $table.append($fragment);
  put2ToRandomCell();
  draw();
}

function put2ToRandomCell() {
  // data.flat().filter((v) => v === 0); 배열을 1차원으로 줄여서 filter로 0을 찾아냄
  const emptyCells = []; //빈배열을 만들어서 //[[i1j1][i2j2][i3j3]]
  data.forEach((rowData, i) => {
    rowData.forEach((cellData, j) => {
      if (!cellData) emptyCells.push([i, j]); //rowData에 cellData가없으면(cellData값이 0 이면) rowIndex(몇번째줄)cellIndex(몇번째칸)인지를 넣음
    });
  });
  const randomCell = emptyCells[Math.floor(Math.random() * emptyCells.length)]; // 반복문을돌려서data의 비어있는칸을 찾아 emptyCells에 넣고 랜덤돌려서 emptyCells 인덱스를 랜덤하게 뽑음
  data[randomCell[0]][randomCell[1]] = 2;
  // ?
}
function draw() {
  data.forEach((rowData, i) => {
    rowData.forEach((cellData, j) => {
      const $target = $table.children[i].children[j]; //children[i] =tr / children[j] = td
      if (cellData > 0) {
        $target.textContent = cellData;
        $target.className = "color-" + cellData;
      } else {
        $target.textContent = "";
        $target.className = "";
      }
    });
  });

  startGame();
}
